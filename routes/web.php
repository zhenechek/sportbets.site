<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('admin.index'));
});

Auth::routes(['verify' => true]);

Route::prefix('admin')
    ->name('admin.')
    ->namespace('Admin')
    ->middleware(['web' , 'auth'])
    ->group(function(){

        Route::get('/', 'DashboardController@index')->name('index');

        Route::name('link.')
        ->prefix('source')
        ->group(function(){
            Route::get('/', 'LinkController@index')->name('index');
            Route::get('create', 'LinkController@create')->name('create');
            Route::post('store', 'LinkController@store')->name('store');
            Route::get('/edit/{id}', 'LinkController@edit')->name('edit');
            Route::get('/delete/{id}', 'LinkController@destroy')->name('delete');
            Route::post('/update', 'LinkController@update')->name('update');
        });

        Route::name('user.')
        ->prefix('user')
        ->middleware('role:super-admin')
        ->group(function(){
            Route::get('/', 'UserController@index')->name('index');
            Route::get('/edit/{id}', 'UserController@edit')->name('edit');
            Route::get('/create', 'UserController@create')->name('create');
            Route::post('/create', 'UserController@store')->name('store');
            Route::post('/update', 'UserController@update')->name('update');
            Route::get('/delete/{id}', 'UserController@destroy')->name('delete');
        });

        Route::name('postback.')
        ->prefix('postback')
        ->group(function(){
            Route::get('/', 'PostbackController@index')->name('index');
            Route::get('/create/{source}', 'PostbackController@create')->name('create');
            Route::post('/create', 'PostbackController@store')->name('store');

        });

        Route::name('statistic.')
        ->prefix('statistic')
        ->group(function(){
            Route::get('/', 'StatisticController@index')->name('index');
        });

    });
