<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::name('domain.')
->namespace('Api')
->prefix('domain')
->group(function(){
    Route::post('save', 'DomainController@save')->name('save');
    Route::get('get', 'DomainController@get')->name('get');
});

Route::name('link.')
->namespace('Api')
->prefix('link')
->group(function(){
    // Route::post('save', 'LinkController@save')->name('save');
    Route::get('get', 'LinkController@index')->name('get');
});
