@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
                <h1>Create source</h1>
                <form action="{{ route('admin.link.store') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="source" class="col-sm-2 col-form-label">Source</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="source" name="source" placeholder="Source" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="campaign" class="col-sm-2 col-form-label">Campaign</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="campaign" placeholder="Campaign" name="campaign" >
                        </div>
                    </div>
                    @role('super-admin')
                        <fieldset class="form-group">
                            <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">User</legend>
                            <div class="col-sm-10">
                                @foreach ($users as $user)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="user_id" value="{{ $user->id}}">
                                        <label class="form-check-label" for="gridRadios1">
                                            {{ $user->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <div class="row">
                                    <label for="campaign_1x" class="col-sm-2 col-form-label">Campaign_1x</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="campaign_1x" placeholder="Campaign_1x" name="campaign_1x" >
                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                    <label for="url" class="col-sm-2 col-form-label">Url</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="url" placeholder="Url" name="url" >
                                    </div>
                            </div>
                        </div>
                    @else
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    @endrole
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{ route('admin.link.index') }}" class="btn btn-outline-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>
@endsection

