@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
                <h1>Users</h1>
                <a name="" id="" class="btn btn-outline-primary mb-3 float-right" href="{{ route('admin.user.create') }}" role="button">Create user</a>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td scope="row">{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->roles[0]->name }}</td>
                                        <td>
                                            <a class="btn btn-outline-primary" href="{{ route('admin.user.edit', $user->id) }}" role="button">Update</a>
                                            <a class="btn btn-outline-danger" href=" {{ route('admin.user.delete', $user->id) }}" role="button">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
@endsection

