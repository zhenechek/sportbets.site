@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1>Statistics</h1>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                        <thead class="thead-inverse">
                            <tr>
                                <th>SubID</th>
                                <th>Clicks</th>
                                <th>Regs</th>
                                <th>Dep</th>
                                <th>Curr</th>
                                <th>Prof</th>
                                <th>Comm</th>
                                <th>CountRegDep</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($statistics as $item)
                                <tr>
                                    <td>{{ $item->SubID }}</td>
                                    <td>{{ $item->DirectLink }}</td>
                                    <td>{{ $item->Regs }}</td>
                                    <td>{{ $item->SumDep }}</td>
                                    <td>{{ $item->currency }}</td>
                                    <td class="{{ $item->SumProf > 0 ? 'bg-success text-white' : 'bg-danger text-white' }}">{{ $item->SumProf }}</td>
                                    <td>{{ $item->ItogComission }}</td>
                                    <td>{{ $item->CountRegDep }}</td>
                                    <td>{{ $item->updated_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="mx-auto">
            {{ $statistics->links() }}
        </div>
    </div>
</div>
@endsection

