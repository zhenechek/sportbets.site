@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
                <h1>Edit source</h1>
                <form action="{{ route('admin.link.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $link->id }}">
                    <fieldset class="form-group">
                            <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">User</legend>
                            <div class="col-sm-10">
                                @foreach ($users as $user)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="user_id" value="{{ $user->id }}" {{ $user->id === $link->user->id ? 'checked' : '' }}>
                                        <label class="form-check-label" for="user_id">
                                            {{ $user->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            </div>
                        </fieldset>
                    <div class="form-group row">
                        <label for="source" class="col-sm-2 col-form-label">Source</label>
                        <div class="col-sm-10">
                        <input type="name" class="form-control" id="source" name="source" placeholder="Source" value="{{ $link->source }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="campaign" class="col-sm-2 col-form-label">Campaign</label>
                        <div class="col-sm-10">
                        <input type="name" class="form-control" id="campaign" name="campaign" placeholder="Campaign" value="{{ $link->campaign }}">
                        </div>
                    </div>
                    @role('super-admin')
                    <div class="form-group row">
                            <label for="source_1x" class="col-sm-2 col-form-label">Source 1x</label>
                            <div class="col-sm-10">
                            <input type="name" class="form-control" id="source_1x" name="source_1x" placeholder="Source 1x" value="{{ $link->source_1x }}">
                            </div>
                        </div>
                    <div class="form-group row">
                            <label for="campaign_1x" class="col-sm-2 col-form-label">Campaign 1x</label>
                            <div class="col-sm-10">
                            <input type="name" class="form-control" id="campaign_1x" name="campaign_1x" placeholder="Campaign 1x" value="{{ $link->campaign_1x }}">
                            </div>
                        </div>
                    <div class="form-group row">
                        <label for="url" class="col-sm-2 col-form-label">Url</label>
                        <div class="col-sm-10">
                                <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">{{ $domain }}</span>
                                </div>
                                <input type="text" class="form-control" id="url" name="url" placeholder="Url" value="{{ $link->url }}">
                                </div>
                        </div>
                    </div>
                    <fieldset class="form-group">
                            <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Active</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="is_active" value="1" {{ $user->is_active == true ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gridRadios1">
                                        Active
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="is_active" value="0" {{ $user->is_active == false ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gridRadios1">
                                        Non active
                                    </label>
                                </div>
                            </div>
                            </div>
                        </fieldset>
                    @endrole
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{ route('admin.link.index') }}" class="btn btn-outline-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
                <h2 class="mt-5">Postback</h2>
                <a name="" id="" class="btn btn-outline-primary mb-3 float-right" href="{{ route('admin.postback.create', $link->id) }}" role="button">Create postback</a>
                <table class="table table-striped table-hover">
                        <thead class="thead-inverse">
                            <tr>
                                <th>#</th>
                                <th>Conversion Type</th>
                                <th>Url</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postbacks as $postback)
                                <tr>
                                    <td scope="row">{{ $postback->id }}</td>
                                    <td>{{ $postback->event->name }}</td>
                                    <td>{{ $postback->url }}</td>
                                    <td>{{ $postback->is_active ? 'Active' : 'Non active' }}</td>
                                    {{-- <td>
                                        <a class="btn btn-outline-primary" href="{{ route('admin.link.edit', $link->id) }}" role="button">Update</a>
                                        <a class="btn btn-outline-danger" href=" {{ route('admin.link.delete', $link->id) }}" role="button">Delete</a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                </table>
        </div>
    </div>
</div>
@endsection

