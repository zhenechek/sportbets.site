@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
                <h1>Sources</h1>
                <a name="" id="" class="btn btn-outline-primary mb-3 float-right" href="{{ route('admin.link.create') }}" role="button">Create source</a>
                <div class="table-responsive">
                        <table class="table table-striped table-hover">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>#</th>
                                        @role('super-admin')
                                            <th>User</th>
                                            <th>Campaign 1x</th>
                                        @endrole
                                        <th>Source</th>
                                        <th>Campaign</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- {{ dd($links) }} --}}
                                    @foreach ($links as $link)
                                        <tr>
                                            <td scope="row">{{ $link->id }}</td>
                                            @role('super-admin')
                                                <td>{{ $link->user->name }}</td>
                                                <td>{{ $link->campaign_1x }}</td>
                                            @endrole
                                            <td>{{ $link->source }}</td>
                                            <td>{{ $link->campaign }}</td>
                                            <td>{{ $link->is_active ? 'Active' : 'Non active' }}</td>
                                            <td>
                                                <a class="btn btn-outline-primary" href="{{ route('admin.link.edit', $link->id) }}" role="button">Update</a>
                                                <a class="btn btn-outline-danger" href=" {{ route('admin.link.delete', $link->id) }}" role="button">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
        </div>
    </div>
</div>
@endsection

