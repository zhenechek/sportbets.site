@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
                <h1>Edit user</h1>
                <form action="{{ route('admin.user.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Campaign name</label>
                        <div class="col-sm-10">
                        <input type="name" class="form-control" id="inputEmail3" name="name" placeholder="Campaign name" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" value="{{ $user->email }}">
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Role</legend>
                        <div class="col-sm-10">
                            @foreach ($roles as $role)
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="role_id" value="{{ $role->id}}" {{ $role->id === $user->roles[0]->id ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gridRadios1">
                                        {{ $role->name }}
                                    </label>
                                    </div>
                            @endforeach
                        </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                            <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Deleted</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="is_deleted" value="0" {{ $user->is_deleted === false ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gridRadios1">
                                        Active
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="is_deleted" value="1" {{ $user->is_deleted === true ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gridRadios1">
                                        Deleted
                                    </label>
                                </div>
                            </div>
                            </div>
                        </fieldset>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{ route('admin.user.index') }}" class="btn btn-outline-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>
@endsection

