@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
                <h1>Edit user</h1>
                <form action="{{ route('admin.postback.store') }}" method="POST">
                    @csrf
                    {{-- {{ dd($link) }} --}}
                    <input type="hidden" name="link_id" value="{{ $link->id }}">
                    <div class="form-group row">
                        <label for="source" class="col-sm-2 col-form-label">Source name</label>
                        <div class="col-sm-10">
                        <input type="source" class="form-control" id="source" name="source" value="{{ $link->source }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="campaign" class="col-sm-2 col-form-label">Source campaign</label>
                        <div class="col-sm-10">
                        <input type="source" class="form-control" id="campaign" name="campaign" value="{{ $link->campaign }}" readonly>
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Conversion event</legend>
                        <div class="col-sm-10">
                            @foreach ($events as $event)
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="event_{{ $event->id }}" name="event_id" value="{{ $event->id }}">
                                    <label class="form-check-label" for="event_{{ $event->id }}">
                                        {{ $event->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Postback url</label>
                        <div class="col-sm-10">
                        <input type="url" class="form-control" id="url" placeholder="Postback url" name="url" >
                        <small id="passwordHelpInline" class="text-muted">
                            Click id заключать в {click_id}. Например https://postback.net/convertion?click_id={click_id}
                        </small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{ route('admin.link.index') }}" class="btn btn-outline-secondary">Cancel</a>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>
@endsection

