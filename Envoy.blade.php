@setup
    $user = 'root';
    $timezone = 'Europe/Moscow';

    $path = '/var/www/www-root/data/www/sportbets.site';

    $current = $path . '/current';

    $repo = 'git@bitbucket.org:zhenechek/sportbets.site.git';

    $branch = 'master';

    $chmods = ['storage/logs'];

    $now = new DateTime();

    $release = $path . '/releases/'.$now->format('YmdHis');

@endsetup

@servers(['production' => $user . '@148.251.153.250'])

@task("pull", ["on" => $on])
    mkdir -p {{ $path }}
    git clone --depth=1 -b {{ $branch }} "{{ $repo }}" {{ $release }}

    echo "#1 - Repository has been cloned"
@endtask

@task("composer", ["on" => $on])
    composer self-update
    cd {{ $release }}
    composer install --no-dev --no-interaction --prefer-dist

    npm update
    npm run dev

    echo "#2 - Composer and npm has been installed"
@endtask

@task("artisan", ["on" => $on])
    cd {{ $release }}
    ln -nfs {{ $path }}/.env .env
    chgrp -h root .env

    php artisan config:clear
    php artisan cache:clear
    {{-- php artisan migrate:refresh --seed --}}
    php artisan clear-compiled --env=production
    {{-- php artisan optimize --env=production --}}

    echo "#3 - Prod dependencies has been installed"
@endtask

@task("chmod", ["on" => $on])
    chgrp -R www-data {{ $release }};
    chmod -R 775 {{ $release }};

    @foreach($chmods as $file)
        chmod -R 777 {{ $release }}/{{ $file }}
        chgrp -R www-data {{ $release }}/{{ $file }}

        echo "Permission have been set for {{ $file }}"
    @endforeach

    chmod -R 777 {{ $release }}/storage

    echo "#4 - Permission has been set"
@endtask

@task("update-symlinks", ["on" => $on])

    ln -nfs {{ $release }} {{ $current }}
    chgrp -h root {{ $current }}

    echo "#5 - Symlinks has been updated"
@endtask

@task("npm" , ["on" => $on])
    cd {{ $release }}

    php artisan cache:clear

    echo "#6 - Npm has been completed"
@endtask



@macro("deploy", ["on" => "production"])
    pull
    composer
    artisan
    chmod
    update-symlinks
    npm
@endstory

