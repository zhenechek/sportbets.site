<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsAndRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'set source']);
        Permission::create(['name' => 'delete source']);
        Permission::create(['name' => 'update source']);
        Permission::create(['name' => 'view statistics']);
        Permission::create(['name' => 'publish url']);
        Permission::create(['name' => 'unpublish url']);

        // create roles and assign created permissions

        // or may be done by chaining
        $role = Role::create(['name' => 'buyer'])
            ->givePermissionTo(['set source', 'delete source', 'update source', 'view statistics']);

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
