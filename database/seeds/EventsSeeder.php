<?php

use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'name' => 'install',
        ]);
        DB::table('events')->insert([
            'name' => 'registration',
        ]);
    }
}
