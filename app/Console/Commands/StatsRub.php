<?php

namespace App\Console\Commands;

use App\Link;
use App\Postback;
use App\SendedPostback;
use App\Statistic;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class StatsRub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:rub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check rub 1x stat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $base_url = 'https://partners1xbet.com/API/GetPostbackSubId?token=W0ImTxAme6Tix1q79zg63T79aR782U6g01S4J419JK5R6D27sgLijayD3km02FGJYmHTk1Jm83g91d3W49CzI389f619zBSe11s18L3owz5AU99eBPpgch0p5Ho7zkN75buAKEZaPocj88F26AX3LM&MerchantId=3&dateStart='.strtotime(date('Y-m-d')).'&dateEnd='.strtotime(date('Y-m-d'));

        $client = new Client();

        $data = json_decode($client->get($base_url)->getBody()->getContents());

        foreach($data as $item){
            $statistic = Statistic::updateOrCreate(
                [
                    'SubID' => $item->SubID,
                    'currency' => 'rub'
                ],
                [
                    'SubID' => $item->SubID,
                    'currency' => 'rub',
                    'DirectLink' => $item->DirectLink,
                    'Regs' => $item->Regs,
                    'SumDep' => $item->SumDep,
                    'SumProf' => $item->SumProf,
                    'ItogComission' => $item->ItogComission,
                    'CountRegDep' => $item->CountRegDep,
                ]
            );

            $this->info(json_encode($item));

            if($item->DirectLink > 0) {
                $sub = explode("-", $item->SubID);
                if(count($sub) == 4 && Link::where('source', $sub[1])->count() > 0) {
                    $username = $sub[0];
                    $source = $sub[1];
                    $campaign = $sub[2];
                    $click_id = $sub[3];

                    $link_id = Link::where('source_1x', $source)
                    ->where('campaign_1x', $campaign)
                    ->where('is_active', true)
                    ->first();


                    if($link_id->count() > 0) {
                        $postback = Postback::where('link_id', $link_id->id)->first();
                        $send_postback = new SendedPostback();
                        $send_postback->link_id = $link_id->id;
                        $send_postback->user_id = $link_id->user_id;
                        $send_postback->url = preg_replace('/{click_id}/', $click_id, $postback->url);

                        $send_postback->save();
                    }
                }
            }
        }

    }
}
