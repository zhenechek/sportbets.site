<?php

namespace App\Console\Commands;

use App\SendedPostback;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class SendPostbacks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:postbacks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending postbacks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $postbacks = SendedPostback::where('is_sended', false)->get();

        $client = new Client();

        foreach($postbacks as $postback) {

            $response = $client->get($postback->url);
            if($response->getStatusCode() == 200)
            {
                $postback->response = $response->getBody()->getContents();
                $postback->is_sended = true;

                $postback->save();

            }
        }

    }
}
