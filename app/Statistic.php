<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable = [
        'currency',
        'SubID',
        'DirectLink',
        'Regs',
        'SumDep',
        'SumProf',
        'ItogComission',
        'CountRegDep',
    ];
}
