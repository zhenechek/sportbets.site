<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Postback extends Model
{
    /**
     * A role belongs to some users of the model associated with its guard.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(
            'App\Event'
        );
    }

    public function link(): BelongsTo
    {
        return $this->belongsTo(
            'App\Link'
        );
    }
}
