<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SendedPostback extends Model
{
    public function link(): BelongsTo
    {
        return $this->belongsTo(
            'App\Link'
        );
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            'App\User'
        );
    }
}
