<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Link extends Model
{

    /**
     * A role belongs to some users of the model associated with its guard.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(
            'App\User'
        );
    }

    /**
     * A role belongs to some users of the model associated with its guard.
     */
    public function postback(): BelongsTo
    {
        return $this->belongsTo(
            'App\Postback'
        );
    }
}
