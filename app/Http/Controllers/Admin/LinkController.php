<?php

namespace App\Http\Controllers\Admin;

use App\Domain;
use App\Link;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Postback;
use App\User;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(auth()->user()->getRoleNames()[0] === 'super-admin') $links = Link::with('user')->where('is_deleted', false)->get();
        else $links = Link::with('user')->where('user_id', auth()->user()->id)->where('is_deleted', false)->orderBy('id', 'ASC')->get();


        return view('admin.links')->with(compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = [];
        $domain = '';

        if(auth()->user()->getRoleNames()[0] === 'super-admin') {
            $domain = Domain::find(1)->name;
            $users = User::role('buyer')->get();
        }

        return view('admin.links_create')->with(compact('users', 'domain'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $link = new Link();

        $link->user_id = $request->get('user_id');
        $link->source = $request->get('source');
        $link->campaign = $request->get('campaign');
        $link->campaign_1x = $request->get('campaign_1x');
        $link->source_1x = $request->get('source_1x');
        $link->url = $request->get('url');

        $link->save();

        return redirect(route('admin.link.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = [];
        $domain = '';

        $link = Link::with(['user', 'postback'])->find($id);
        if(auth()->user()->getRoleNames()[0] === 'super-admin') {

            $users = User::role('buyer')->get();
            $domain = Domain::first()->name;

        }

        $postbacks = Postback::where('link_id', $id)->with('event')->get();

        return view('admin.links_edit')->with(compact('link', 'users', 'postbacks', 'domain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {
        $upd = $link->find($request->get('id'));

        $upd->source = $request->get('source');
        $upd->campaign = $request->get('campaign');
        $upd->campaign_1x = $request->get('campaign_1x');
        $upd->source_1x = $request->get('source_1x');
        $upd->url = $request->get('url');
        $upd->is_active = $request->get('is_active');

        $upd->save();

        return redirect(route('admin.link.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link = Link::find($id);
        $link->is_deleted = true;
        $link->is_active = false;
        $link->save();

        return redirect()->back();
    }
}
