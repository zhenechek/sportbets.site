<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Postback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Link;
use App\SendedPostback;

class PostbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SendedPostback::with(['user'])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($source)
    {

        $link = Link::find($source);
        $events = Event::all();

        return view('admin.postbacks_create')->with(compact('link', 'events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'url' => 'url',
            'event_id' => 'required'
        ]);

        $postback = new Postback();
        $postback->link_id = $request->get('link_id');
        $postback->event_id = $request->get('event_id');
        $postback->url = $request->get('url');
        $postback->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Postback  $postback
     * @return \Illuminate\Http\Response
     */
    public function show(Postback $postback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Postback  $postback
     * @return \Illuminate\Http\Response
     */
    public function edit(Postback $postback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Postback  $postback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Postback $postback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Postback  $postback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Postback $postback)
    {
        //
    }
}
