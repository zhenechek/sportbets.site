<?php

namespace App\Http\Controllers\Api;

use App\Domain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Link;
use App\User;
use InvalidArgumentException;
use PHPUnit\Framework\InvalidDataProviderException;

class LinkController extends Controller
{
    public function index(Request $request)
    {

        $link = Link::with('user')->where('source', $request->get('pid'))
        ->where('campaign', $request->get('c'))
        ->first();
        if(!$link) throw new InvalidDataProviderException('no data');

        $domain = Domain::first()->name;

        $param = $link->user->name . '-' . $link->source_1x . '-' . $link->campaign_1x . '-' . $request->get('click_id');

        $url = $link->url;
        $url = preg_replace('/{param}/', $param, $url);
        $result = $domain.$url;

        return $result;

    }
}
