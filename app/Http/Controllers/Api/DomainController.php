<?php

namespace App\Http\Controllers\Api;

use App\Domain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DomainController extends Controller
{
    public function save(Request $request)
    {

        $domail = Domain::updateOrCreate(
            ['id' => 1],
            ['name' => $request->get('name')]
        );

        return $domail;
    }

    public function get()
    {
        return Domain::find(1)->name;
    }
}
